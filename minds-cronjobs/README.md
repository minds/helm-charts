### Minds Cron Jobs

#### Installing

```
helm install minds-cronjobs .
```

### Changing the image of a job

It is recommended to change the yaml file instead of using the `--set` options.

Simply change (or add) the imageTag variable for the job you want to modify. Commnit the change and push up to this repository. It should automatically apply the changes

### Viewing a list of cronjobs

```
kubectl get cronjobs
```

### Manually upgrading

```
helm upgrade --values=values.yaml minds-cronjobs .
```