FROM alpine/helm:3.5.4

RUN apk update \
    && apk add --no-cache py-pip git npm \
    && pip install --upgrade pip \
    && pip install awscli

RUN npm i -g @sentry/cli --unsafe-perm

RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
    chmod +x kubectl && \
    mkdir -p ~/.local/bin/kubectl && \
    mv ./kubectl ~/.local/bin/kubectl

ENTRYPOINT ["sh", "-c"]