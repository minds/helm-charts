# Minds Helm Chart

This helm chart runs the minds web app and backend services. It does not install or manage the databases. 

# Setup

- Install Helm (https://docs.helm.sh/using_helm/#installing-helm)
- `helm install RELEASE_NAME .`

# Improving working directory mount performance

```
docker run -v /Users/Mark/Documents/Minds/:/var/www/Minds:delegated alpine sleep 1d
```

# Creating encryption keys

```
openssl genrsa -des3 -out ./certs/private.pem 4096
openssl rsa -in ./certs/private.pem -outform PEM -pubout -out ./certs/public.pem
```
