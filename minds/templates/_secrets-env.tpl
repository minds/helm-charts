
{{- /*
Prefix the environment variables with MINDS_ENV_. All others are ignored
Suffix the environment variables with the key in Config.php
Nest arrays with {prefix}{key}__subkey__{...}_{Config Key}
Spacing matters. Be sure to --dry-run, --debug 
The errors that come from helm regarding yaml formatting issues are inscrutable
You have been warned. 
*/}}
{{- define "minds.secrets-env" }}
            - name: MINDS_ENV_twilio__account_sid
              valueFrom:
                secretKeyRef:
                  name: twilio-account-sid
                  key: twilio-account-sid
            - name: MINDS_ENV_twilio__auth_token
              valueFrom:
                secretKeyRef:
                  name: twilio-auth-token
                  key: twilio-auth-token
            - name: MINDS_ENV_iframely__key
              valueFrom:
                secretKeyRef:
                  name: iframely-key
                  key: iframely-key
            - name: MINDS_ENV_payments__stripe__api_key
              valueFrom:
                secretKeyRef:
                  name: stripe-api-key
                  key: stripe-api-key
            - name: MINDS_ENV_thechecker_secret
              valueFrom:
                secretKeyRef:
                  name: thechecker-secret
                  key: thechecker-secret
            - name: MINDS_ENV_captcha__bypass_key
              valueFrom:
                secretKeyRef:
                  name: captcha-bypass-key
                  key: captcha-bypass-key
            - name: MINDS_ENV_cypress__shared_key
              valueFrom:
                secretKeyRef:
                  name: cypress-shared-key
                  key: cypress-shared-key
            - name: MINDS_ENV_blockchain__rpc_endpoints__0
              valueFrom:
                secretKeyRef:
                  name: blockchain-rpc-endpoint
                  key: blockchain-rpc-endpoint
            ## Sendgrid setup ##
            - name: MINDS_ENV_email__smtp__host
              valueFrom:
                secretKeyRef:
                  name: email-smtp
                  key: host
            - name: MINDS_ENV_email__smtp__username
              valueFrom:
                secretKeyRef:
                  name: email-smtp
                  key: username
            - name: MINDS_ENV_email__smtp__password
              valueFrom:
                secretKeyRef:
                  name: email-smtp
                  key: password
            - name: MINDS_ENV_google__youtube__client_id
              valueFrom:
                secretKeyRef:
                  name: google-client-id
                  key: google-client-id
            - name: MINDS_ENV_google__youtube__client_secret
              valueFrom:
                secretKeyRef:
                  name: google-client-secret
                  key: google-client-secret
            - name: MINDS_ENV_google__youtube__api_key
              valueFrom:
                secretKeyRef:
                  name: yt-api-key
                  key: yt-api-key
{{- end }}
